﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static My_Metrics;

public class Open_Task : MonoBehaviour
{
    public TextMesh o_t_value;

    // Start is called before the first frame update
    void Start()
    {
        M_M_Server cds = new M_M_Server();
        RootObject root = cds.CallServer();

        o_t_value.text = root.OT.o_t_value;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
