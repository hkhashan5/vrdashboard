﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Test1 : MonoBehaviour
{
    private bool shouldlerp = false;
    public float timeStartedLerping;
    public float lerpTime;



    public Vector2 endposition;
    public Vector2 startposition;

    private void StartLerping()
    {
        timeStartedLerping = Time.time;

        Lerp(startposition, endposition, timeStartedLerping, lerpTime);

        shouldlerp = true;
    }


    // Start is called before the first frame update
    void Start()
    {
        StartLerping();
    }

    // Update is called once per frame
    void Update()
    {
        if (shouldlerp)

        {
            transform.position = Lerp(startposition, endposition, timeStartedLerping, lerpTime);
        }
        
    }
    public Vector3 Lerp(Vector3 start, Vector3 end, float timeStartedLerping, float Lerptime = 1)
    {
        float timeSinceStarted = Time.time - timeStartedLerping;

        float percentageComplete = timeSinceStarted / Lerptime;

        var result = Vector3.Lerp(start, end, percentageComplete);
        return result;

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.name == "Cube")
        {
            Debug.Log("Collider name is cube");
        }
    }


}
