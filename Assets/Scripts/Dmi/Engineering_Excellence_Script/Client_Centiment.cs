﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class Client_Centiment : MonoBehaviour
{

    public TextMesh negative;
    public TextMesh positive;

    // Start is called before the first frame update
    void Start()
    {
        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();

        negative.text = root.VOC.CS.positive; 
        positive.text = root.VOC.CS.negative;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
