﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide_Dmi_Floor : MonoBehaviour
{
    public GameObject Floor1;
    public GameObject Floor2;



    // Start is called before the first frame update
    void Start()
    {
        Floor1.GetComponent<Renderer>().enabled = false;
        Floor2.GetComponent<Renderer>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {

    }
}

