﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane6_Move : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 5.5f;

    void Start()
    {
        endPosition = new Vector3(45f, 150f, 500f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
