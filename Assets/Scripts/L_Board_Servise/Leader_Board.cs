﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leader_Board : MonoBehaviour
{
    [System.Serializable]
    public class MAN1
    {
        public string name_Value1;
        public string point_Value1;
        public string rank_Value1;
    }

    [System.Serializable]
    public class MAN2
    {
        public string name_Value2;
        public string point_Value2;
        public string rank_Value2;
    }

    [System.Serializable]
    public class MAN3
    {
        public string name_Value3;
        public string point_Value3;
        public string rank_Value3;
    }

    [System.Serializable]
    public class MAN4
    {
        public string name_Value4;
        public string point_Value4;
        public string rank_Value4;
    }

    [System.Serializable]
    public class MAN5
    {
        public string name_Value5;
        public string point_Value5;
        public string rank_Value5;
    }

    [System.Serializable]
    public class RootObject
    {
        public MAN1 MAN1;
        public MAN2 MAN2;
        public MAN3 MAN3;
        public MAN4 MAN4;
        public MAN5 MAN5;


    }

}
