﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testone : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 3.54f;

    void Start() {

        endPosition = new Vector3(3.4f, 0.93f, 9f);
    }

    void Update()
    {
        if(gameObject.transform.position.z == 9f)
        {
            endPosition = new Vector3(3.4f, 1000f, 9f);

        }
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
