﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane8_Move : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 15f;

    void Start()
    {
        endPosition = new Vector3(62.5f, 150f, 500f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
