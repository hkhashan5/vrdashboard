﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Red_Stone : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 5.0f;

    void Start()
    {
        endPosition = new Vector3(17.5f, -999f, 100f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
