﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Load_Home : MonoBehaviour
{

    public GameObject Cube;
    public GameObject Plane;


    // Start is called before the first frame update
    void Start()
    {
        Plane.GetComponent<Renderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter(UnityEngine.Collision collision)
    {

        if (collision.collider.name == "Sphere")
        {
            //StartCoroutine(ExecuteAfterTime(2));
            Destroy(gameObject);
            SceneManager.LoadScene(1);
        }


    }
}


