﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide_M_M_Floor : MonoBehaviour
{
    public GameObject Floor;
    public GameObject Floor1;



    // Start is called before the first frame update
    void Start()
    {
        Floor.GetComponent<Renderer>().enabled = false;
        Floor1.GetComponent<Renderer>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
