﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L_Board_Move : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 5.0f;

    void Start()
    {
        endPosition = new Vector3(3.87f, 0.6f, 9.85f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
