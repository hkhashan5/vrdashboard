﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class Execute_R_R : MonoBehaviour
{
    public TextMesh percentage;
    public TextMesh met;
    public TextMesh not_met;
    public TextMesh no_data;
    public TextMesh overdue;

    // Start is called before the first frame update
    void Start()
    {
        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();

        percentage.text = root.GOV.ERR.percentage;
        met.text = root.GOV.ERR.met;
        not_met.text = root.GOV.ERR.not_met;
        no_data.text = root.GOV.ERR.no_data;
        overdue.text = root.GOV.ERR.overdue;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
