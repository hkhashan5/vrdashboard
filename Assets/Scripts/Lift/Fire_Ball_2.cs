﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire_Ball_2 : MonoBehaviour
{

    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 10.0f;

    void Start()
    {
        endPosition = new Vector3(48f, -999f, 80f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
