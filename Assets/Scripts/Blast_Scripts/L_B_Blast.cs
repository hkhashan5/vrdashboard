﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L_B_Blast : MonoBehaviour
{
    public GameObject Floor1;
    public float cubeSize = 0.2f;
    public int cubesInrow = 5;

    public float explosionRadius = 4f;
    public float explosionForce = 300f;
    public float explosionUpward = 0.4f;

    float cubesPivotDistance;
    Vector3 cubesPivot;

    // Start is called before the first frame updateDMI_Blast
    void Start()
    {
        // Floor.GetComponent<Renderer>().enabled = false;

        cubesPivotDistance = cubeSize * cubesInrow / 2;
        cubesPivot = new Vector3(cubesPivotDistance, cubesPivotDistance, cubesPivotDistance);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Floor1")
        {
            explode();
        }
    }
    public void explode()
    {
        gameObject.SetActive(false);

        for (int x = 0; x < cubesInrow; x++)
        {
            for (int y = 0; y < cubesInrow; y++)
            {
                for (int z = 0; z < cubesInrow; z++)
                {
                    createPiece(x, y, z);
                }
            }
        }
        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, explosionRadius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius, explosionUpward);
            }
        }

    }

    void createPiece(int x, int y, int z)
    {
        GameObject piece;
        piece = GameObject.CreatePrimitive(PrimitiveType.Cube);

        piece.transform.position = transform.position + new Vector3(cubeSize * x, cubeSize * y, cubeSize * z) - cubesPivot;
        piece.transform.localScale = new Vector3(cubeSize, cubeSize, cubeSize);

        piece.AddComponent<Rigidbody>();
        piece.GetComponent<Rigidbody>().mass = cubeSize;

    }
}
