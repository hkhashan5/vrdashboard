﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class Start_Right_R : MonoBehaviour
{
    public TextMesh ontrack;
    public TextMesh offtrack;
    public TextMesh percentage;

    // Start is called before the first frame update
    void Start()
    {
        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();

        ontrack.text = root.GOV.SRR.ontrack;
        offtrack.text = root.GOV.SRR.offtrack;
        percentage.text = root.GOV.SRR.percentage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
