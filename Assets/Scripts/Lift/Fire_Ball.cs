﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire_Ball : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 10.0f;

    void Start()
    {

        endPosition = new Vector3(-19.5f, -999f, 57.3f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
