﻿//======= Copyright (c) Valve Corporation, All rights reserved. ===============
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Valve.VR.Extras
{
    public class SteamVR_LaserPointer : MonoBehaviour
    {
        public SteamVR_Behaviour_Pose pose;

        //public SteamVR_Action_Boolean interactWithUI = SteamVR_Input.__actions_default_in_InteractUI;
        public SteamVR_Action_Boolean interactWithUI = SteamVR_Input.GetBooleanAction("InteractUI");

        public bool active = true;
        public Color color;
        public float thickness = 0.002f;
        public Color clickColor = Color.green;
        public GameObject holder;
        public GameObject pointer;
        bool isActive = false;
        public bool addRigidBody = false;
        public Transform reference;
        public event PointerEventHandler PointerIn;
        public event PointerEventHandler PointerOut;
        public event PointerEventHandler PointerClick;

        Transform previousContact = null;

        private bool shouldlerp = false;
        public float timeStartedLerping;
        public float lerpTime;



        public Vector2 endposition;
        public Vector2 startposition;

        // public GameObject[] leaderBoard;


        public Transform[] myMetricsArray;
        public GameObject myMetrics;

        public Transform[] dmiArray;
        public GameObject dmi;

        public Transform[] leaderBoardArray;
        public GameObject leaderBoard;

        private Vector2 startPosition;

        public Vector3 newposition = new Vector3(3.87f, 2.5f, 16.0f);
        public float movementStep = 3f;

        public Vector3 newscale = new Vector3(0.01f, 7f, 9.5f);

        public GameObject back_Icon;
        public bool disbleObject;
        public GameObject front_Icon;

        public string previousScreenName;

        // private object go;

        //  private Vector3 startPosition;
        Vector3 endPosition;
        float speed = 600f;


        private void StartLerping()
        {
            timeStartedLerping = Time.time;

            Lerp(startposition, endposition, timeStartedLerping, lerpTime);

            shouldlerp = true;
        }


        private void Start()
        {
            if (pose == null)
                pose = this.GetComponent<SteamVR_Behaviour_Pose>();
            if (pose == null)
                Debug.LogError("No SteamVR_Behaviour_Pose component found on this object");

            if (interactWithUI == null)
                Debug.LogError("No ui interaction action has been set on this component.");


            holder = new GameObject();
            holder.transform.parent = this.transform;
            holder.transform.localPosition = Vector3.zero;
            holder.transform.localRotation = Quaternion.identity;

            pointer = GameObject.CreatePrimitive(PrimitiveType.Cube);
            pointer.transform.parent = holder.transform;
            pointer.transform.localScale = new Vector3(thickness, thickness, 100f);
            pointer.transform.localPosition = new Vector3(0f, 0f, 50f);
            pointer.transform.localRotation = Quaternion.identity;
            BoxCollider collider = pointer.GetComponent<BoxCollider>();
            if (addRigidBody)
            {
                if (collider)
                {
                    collider.isTrigger = true;
                }
                Rigidbody rigidBody = pointer.AddComponent<Rigidbody>();
                rigidBody.isKinematic = true;
            }
            else
            {
                if (collider)
                {
                    Object.Destroy(collider);
                }
            }
            Material newMaterial = new Material(Shader.Find("Unlit/Color"));
            newMaterial.SetColor("_Color", color);
            pointer.GetComponent<MeshRenderer>().material = newMaterial;

            StartLerping();
        }

        public virtual void OnPointerIn(PointerEventArgs e)
        {
            if (PointerIn != null)
                PointerIn(this, e);
        }

        public virtual void OnPointerClick(PointerEventArgs e)
        {

            if (PointerClick != null)
                PointerClick(this, e);


            switch (e.target.name)
            {
                case "Dmi_Panel":
                    SceneManager.LoadScene("DMI_Blast");
                    break;
                    
                case "Leader_Board_Panel":
                    SceneManager.LoadScene("LeaderBoardBlast");
                    break;

                case "My_Metrics_Panel":
                    SceneManager.LoadScene("M_M_Blast");
                    break;

                case "Home_Icon":
                    SceneManager.LoadScene("Main_Home");
                    break;

                case "Home_Back_Icon":
                    SceneManager.LoadScene("Main_Home");
                    break;

                case "Dmi_Back_Icon":
                    SceneManager.LoadScene("DMI");
                    break;

                case "My_Metrices_Back_Icon":
                    SceneManager.LoadScene("Mymetrices");
                    break;

                case "Voice_O_C_Back_Icon":
                    SceneManager.LoadScene("Voice_Of_client");
                    break;

                case "E_Excellence_Back_Icon":
                    SceneManager.LoadScene("E_Excellence_Main");
                    break;

                case "Governence_Back_Icon":
                    SceneManager.LoadScene("Governance");
                    break;

                case "Leader_Board_Back_Icon":
                    SceneManager.LoadScene("Leader_Board");
                    break;

                case "Voice_Of_Clience_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Voice_Of_client");
                    }
                    break;

                case "Nps_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("NPS_Test");
                    }
                    break;

                case "C_Sentiment_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("C_Sentiment_New");
                    }
                    break;

                case "C_S_Metrics_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("C_S_Metrics_New");
                    }
                    break;

                case "Engineering_excelence_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("E_Excellence_Main");
                    }
                    break;

                case "RCI_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("RCI_Panel_Main");
                    }
                    break;

                case "Unit_Test_Coverage_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("U_T_Coverage_Panel");
                    }
                    break;

                case "CI/CD_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("CI_CD_Main");
                    }
                    break;

                case "Coding_Excellence_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Coding_Excellence");
                    }
                    break;

                case "UAT_DD_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("UAT_DD");
                    }
                    break;

                case "FTR_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("FRT");
                    }
                    break;

                case "Governence_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Governance");
                    }
                    break;

                case "Start_R_R_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Start_Right_R");
                    }
                    break;

                case "Execute_R_R_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Execute_R_R");
                    }
                    break;

                case "Open_A_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Open_Alerts");
                    }
                    break;

                case "Open_Task_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Open_Task");
                    }
                    break;

                case "Open_Defect_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Open_Defect");
                    }
                    break;

                case "Open_Violation_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Open_violation");
                    }
                    break;

                case "Lines_Of_Codes_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Lines_of_Codes");
                    }
                    break;

                case "Productivity_Panel":
                    endPosition = new Vector3(3.87f, 2.48f, 16f);
                    e.target.transform.position = Vector3.MoveTowards(e.target.transform.position, endPosition, speed * Time.deltaTime);

                    if (e.target.transform.position.z >= 15)
                    {
                        SceneManager.LoadScene("Productivity");
                    }
                    break;

            }

        }

        public virtual void OnPointerOut(PointerEventArgs e)
        {
            if (PointerOut != null)
                PointerOut(this, e);
            // isObjectAlreadyScaled = true;
        }

        public Vector3 Lerp(Vector3 start, Vector3 end, float timeStartedLerping, float Lerptime = 1)
        {
            float timeSinceStarted = Time.time - timeStartedLerping;

            float percentageComplete = timeSinceStarted / Lerptime;

            var result = Vector3.Lerp(start, end, percentageComplete);
            return result;

        }

        private void Update()
        {
            // Invoke("DisableObject", 0.0001f);
            if (!isActive)
            {
                isActive = true;
                this.transform.GetChild(0).gameObject.SetActive(true);

            }

            float dist = 100f;

            Ray raycast = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            bool bHit = Physics.Raycast(raycast, out hit);

            if (previousContact && previousContact != hit.transform)
            {
                PointerEventArgs args = new PointerEventArgs();
                args.fromInputSource = pose.inputSource;
                args.distance = 0f;
                args.flags = 0;
                args.target = previousContact;
                OnPointerOut(args);
                previousContact = null;
            }
            if (bHit && previousContact != hit.transform)
            {
                PointerEventArgs argsIn = new PointerEventArgs();
                argsIn.fromInputSource = pose.inputSource;
                argsIn.distance = hit.distance;
                argsIn.flags = 0;
                argsIn.target = hit.transform;
                OnPointerIn(argsIn);
                previousContact = hit.transform;
            }
            if (!bHit)
            {
                previousContact = null;
            }
            if (bHit && hit.distance < 100f)
            {
                dist = hit.distance;
            }

            if (bHit && interactWithUI.GetStateUp(pose.inputSource))
            {
                PointerEventArgs argsClick = new PointerEventArgs();
                argsClick.fromInputSource = pose.inputSource;
                argsClick.distance = hit.distance;
                argsClick.flags = 0;
                argsClick.target = hit.transform;
                OnPointerClick(argsClick);
            }

            if (interactWithUI != null && interactWithUI.GetState(pose.inputSource))
            {
                pointer.transform.localScale = new Vector3(thickness * 5f, thickness * 5f, dist);
                pointer.GetComponent<MeshRenderer>().material.color = clickColor;
            }
            else
            {
                pointer.transform.localScale = new Vector3(thickness, thickness, dist);
                pointer.GetComponent<MeshRenderer>().material.color = color;
            }
            pointer.transform.localPosition = new Vector3(0f, 0f, dist / 2f);
        }

        /*  public void AbleObject()
          {
              gameObject.GetComponent<Renderer>().enabled = true;

          }*/
    }

    public struct PointerEventArgs
    {
        public SteamVR_Input_Sources fromInputSource;
        public uint flags;
        public float distance;
        public Transform target;
    }



    public delegate void PointerEventHandler(object sender, PointerEventArgs e);


}