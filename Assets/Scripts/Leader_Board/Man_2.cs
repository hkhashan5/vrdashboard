﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man_2 : MonoBehaviour
{
    public TextMesh name;
    public TextMesh points;
    public TextMesh rank;

    // Start is called before the first frame update
    void Start()
    {
        name.text = "Udesh Sendanayake";
        points.text = "820";
        rank.text = "2";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
