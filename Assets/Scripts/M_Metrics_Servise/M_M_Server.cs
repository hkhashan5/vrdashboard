﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static My_Metrics;

public class M_M_Server : MonoBehaviour
{
    public RootObject CallServer()
    {
        string uri = String.Format("http://localhost:52830/api/values/MyMetrices");
        WebRequest requestObject = WebRequest.Create(uri);
        requestObject.Method = "GET";
        HttpWebResponse responseObject = null;
        responseObject = (HttpWebResponse)requestObject.GetResponse();

        string strresulttest = null;
        using (Stream stream = responseObject.GetResponseStream())
        {
            StreamReader sr = new StreamReader(stream);
            strresulttest = sr.ReadToEnd();
            sr.Close();

            RootObject root = JsonUtility.FromJson<RootObject>(strresulttest);
            return root;
        }
    }

}
