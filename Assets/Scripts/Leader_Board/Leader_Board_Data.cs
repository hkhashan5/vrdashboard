﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static Leader_Board;

public class Leader_Board_Data : MonoBehaviour
{

    public TextMesh name_Value1;
    public TextMesh point_Value1;
    public TextMesh rank_Value1;
    public TextMesh name_Value2;
    public TextMesh point_Value2;
    public TextMesh rank_Value2;
    public TextMesh name_Value3;
    public TextMesh point_Value3;
    public TextMesh rank_Value3;
    public TextMesh name_Value4;
    public TextMesh point_Value4;
    public TextMesh rank_Value4;
    public TextMesh name_Value5;
    public TextMesh point_Value5;
    public TextMesh rank_Value5;

    // Start is called before the first frame update
    void Start()
    {
        L_Board_Server cds = new L_Board_Server();
        RootObject root = cds.CallServer();

        name_Value1.text = root.MAN1.name_Value1;
        point_Value1.text = root.MAN1.point_Value1;
        rank_Value1.text = root.MAN1.rank_Value1;
        name_Value2.text = root.MAN2.name_Value2;
        point_Value2.text = root.MAN2.point_Value2;
        rank_Value2.text = root.MAN2.rank_Value2;
        name_Value3.text = root.MAN3.name_Value3;
        point_Value3.text = root.MAN3.point_Value3;
        rank_Value3.text = root.MAN3.rank_Value3;
        name_Value4.text = root.MAN4.name_Value4;
        point_Value4.text = root.MAN4.point_Value4;
        rank_Value4.text = root.MAN4.rank_Value4;
        name_Value5.text = root.MAN5.name_Value5;
        point_Value5.text = root.MAN5.point_Value5;
        rank_Value5.text = root.MAN5.rank_Value5;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
