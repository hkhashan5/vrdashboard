﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane1_Move : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 15.0f;

    void Start()
    {

        endPosition = new Vector3(82f, 152f, 1100.94f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
