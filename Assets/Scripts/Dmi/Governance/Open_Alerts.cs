﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class Open_Alerts : MonoBehaviour
{
    public TextMesh code_red;
    public TextMesh potential_C_R;
    public TextMesh ambar_Manage;

    // Start is called before the first frame update
    void Start()
    {
        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();

        code_red.text = root.GOV.OA.code_red;
        potential_C_R.text = root.GOV.OA.potential_C_R;
        ambar_Manage.text = root.GOV.OA.ambar_Manage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
