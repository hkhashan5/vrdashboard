﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane3_Move : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 12.0f;

    void Start()
    {

        endPosition = new Vector3(999f, 68f, 147f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
