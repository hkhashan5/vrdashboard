﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man_1 : MonoBehaviour
{

    public TextMesh name;
    public TextMesh points;
    public TextMesh rank;

    // Start is called before the first frame update
    void Start()
    {
        name.text = "Udheeptha Balasooriya";
        points.text = "830";
        rank.text = "1";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
