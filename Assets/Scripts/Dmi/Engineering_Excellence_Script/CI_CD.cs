﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class CI_CD : MonoBehaviour
{
    public TextMesh percentage;
    public TextMesh met;
    public TextMesh not_met;
    public TextMesh no_Data;


    // Start is called before the first frame update
    void Start()
    {
        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();

        percentage.text = root.EE.CICD.percentage;
        met.text = root.EE.CICD.met;
        not_met.text = root.EE.CICD.not_met;
        no_Data.text = root.EE.CICD.no_Data;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
