﻿using System.Collections;
using System.Collections.Generic;
//using System.Numerics;
using UnityEngine;

public class Hide_My_Metrices : MonoBehaviour {

    public float speed =0.05f;
    public Vector3 targetPosition;

    public bool disbleObject;

    public GameObject childText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        disbleObject = false;

        Invoke("DisableObject", 1.5f);
        
    }

    public void Move()
    {
        targetPosition = new Vector3(3.87f, 0.6f, 10.45f);
       
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        
        Invoke("DisableObject", 1.5f);
    }

    public void DisableObject()
    {
        gameObject.GetComponent<Renderer>().enabled = false;
        childText.GetComponent<Renderer>().enabled = false;
    }

}
