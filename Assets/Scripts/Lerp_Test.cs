﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerp_Test : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 1.0f;

    void Start()
    {
       
        endPosition = new Vector3(-1.4f, 100f, 89.58f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}