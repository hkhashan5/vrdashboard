﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man_4 : MonoBehaviour
{
    public TextMesh name;
    public TextMesh points;
    public TextMesh rank;

    // Start is called before the first frame update
    void Start()
    {
        name.text = "Umith Indula";
        points.text = "725";
        rank.text = "4";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
