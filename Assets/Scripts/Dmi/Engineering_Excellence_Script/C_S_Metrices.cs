﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class C_S_Metrices : MonoBehaviour
{
    public TextMesh define_P;
    public TextMesh n_Define_P;

    // Start is called before the first frame update
    void Start()
    {
        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();

        define_P.text = root.VOC.CSM.define_P;
        n_Define_P.text = root.VOC.CSM.n_Define_P;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
