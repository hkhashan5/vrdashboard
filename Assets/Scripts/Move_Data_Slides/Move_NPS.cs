﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_NPS : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 25f;

    void Start()
    {
        endPosition = new Vector3(3.87f, 2.48f, 16f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
