﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man_5 : MonoBehaviour
{
    public TextMesh name;
    public TextMesh points;
    public TextMesh rank;

    // Start is called before the first frame update
    void Start()
    {
        name.text = "Tharindu silva";
        points.text = "700";
        rank.text = "5";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
