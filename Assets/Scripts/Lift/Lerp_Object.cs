﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Lerp_Object : MonoBehaviour
{
   
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 3.0f;

    void Start()
    {
        endPosition = new Vector3(0f, 1000f, 11.94f);      
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);      
    }
}
