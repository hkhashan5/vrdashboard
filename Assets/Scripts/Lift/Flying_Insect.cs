﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flying_Insect : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 8.0f;

    void Start()
    {
        endPosition = new Vector3(1500f, 1009f, 46f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
