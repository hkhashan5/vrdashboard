﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class My_Metrics : MonoBehaviour
{
    [System.Serializable]
    public class OT
    {
        public string o_t_value;    
    }

    [System.Serializable]
    public class OD
    {
        public string o_d_Value;
    }

    [System.Serializable]
    public class PRO
    {
        public string pro_Value;
    }

    [System.Serializable]
    public class OV
    {
        public string open_violation;
    }

    [System.Serializable]
    public class LOC
    {
        public string lines_of_codes;
    }

    [System.Serializable]
    public class RootObject
    {
        public OT OT;
        public OD OD;
        public PRO PRO;
        public OV OV;
        public LOC LOC;
    }
 
}
