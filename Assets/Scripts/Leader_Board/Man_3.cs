﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Man_3 : MonoBehaviour
{
    public TextMesh name;
    public TextMesh points;
    public TextMesh rank;

    // Start is called before the first frame update
    void Start()
    {
        name.text = "Dilan Jayasuriya";
        points.text = "750";
        rank.text = "3";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
