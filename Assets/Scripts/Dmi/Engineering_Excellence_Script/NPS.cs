﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class NPS : MonoBehaviour
{
    public TextMesh prom;
    public TextMesh n_received;
    public TextMesh passive;
    public TextMesh detractors;
    public TextMesh percentage;

    // Start is called before the first frame update
    void Start()
    {

        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();
        
        prom.text = root.VOC.NPS.prom;
        n_received.text = root.VOC.NPS.n_received;
        passive.text = root.VOC.NPS.passive;
        detractors.text = root.VOC.NPS.detractors;
        percentage.text = root.VOC.NPS.percentage;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
