﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_Ball : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 0.1535f;

    void Start()
    {

        endPosition = new Vector3(-104.6f, -105.8f, 211f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
