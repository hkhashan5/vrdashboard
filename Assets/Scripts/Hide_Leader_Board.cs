﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide_Leader_Board : MonoBehaviour
{
    public GameObject Leader_Board_Panel;
    public bool disbleObject;

    public GameObject childText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Invoke("DisableObject", 1f);
    }

    public void DisableObject()
    { 
        gameObject.GetComponent<Renderer>().enabled = false;
        childText.GetComponent<Renderer>().enabled = false;
    }
}
