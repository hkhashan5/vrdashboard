﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DMI_Move : MonoBehaviour
{

    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 2.4f;

    void Start()
    {
        endPosition = new Vector3(1.84f, 0.6f, 10.45f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
