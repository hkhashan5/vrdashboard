﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static My_Metrics;

public class Productivity : MonoBehaviour
{
    public TextMesh pro_Value;

    // Start is called before the first frame update
    void Start()
    {
        M_M_Server cds = new M_M_Server();
        RootObject root = cds.CallServer();

        pro_Value.text = root.PRO.pro_Value;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
