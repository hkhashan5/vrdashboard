﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide_L_B_Floor : MonoBehaviour
{
    public GameObject Floor;
    public GameObject Floor2;



    // Start is called before the first frame update
    void Start()
    {
        Floor.GetComponent<Renderer>().enabled = false;
        Floor2.GetComponent<Renderer>().enabled = false;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
