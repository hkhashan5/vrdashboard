﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hidden_icon : MonoBehaviour
{

    public GameObject Back_Icon;
    public bool disbleObject;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Invoke("DisableObject", 0.0001f);
    }

    public void DisableObject()
    {
        gameObject.GetComponent<Renderer>().enabled = false;
        
    }
}
