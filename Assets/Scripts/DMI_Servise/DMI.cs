﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DMI : MonoBehaviour
{
    [System.Serializable]
    public class NPS
    {
        public string percentage;
        public string prom;
        public string detractors;
        public string passive;
        public string n_received;     
    }

    [System.Serializable]
    public class CS
    {
        public string negative;
        public string positive;
    }

    [System.Serializable]
    public class CSM
    {
        public string define_P;
        public string n_Define_P;  
    }

    [System.Serializable]
    public class RCI
    {
        public string percentage;
        public string met;
        public string not_met;
        public string no_Data;
    }

    [System.Serializable]
    public class UTC
    {
        public string percentage;
        public string met;
        public string not_met;
        public string no_Data;
    }

    [System.Serializable]
    public class CICD
    {
        public string percentage;
        public string met;
        public string not_met;
        public string no_Data;
    }

    [System.Serializable]
    public class CE
    {
        public string percentage;
        public string met;
        public string not_met;
        public string no_Data;
    }

    [System.Serializable]
    public class UATDD
    {
        public string percentage;
        public string met;
        public string not_met;
        public string no_Data;
    }

    [System.Serializable]
    public class FTR
    {
        public string percentage;
    }

        [System.Serializable]
    public class SRR
    {
        public string ontrack;
        public string offtrack;
        public string percentage;      
    }

    [System.Serializable]
    public class ERR
    {
        public string percentage;
        public string met;
        public string not_met;
        public string no_data;
        public string overdue;
    }

    [System.Serializable]
    public class OA
    {
        public string code_red;
        public string potential_C_R;
        public string ambar_Manage;
    }


    [System.Serializable]
    public class VOC
    {
        public NPS NPS;
        public CS CS;
        public CSM CSM;
    }

    [System.Serializable]
    public class EE
    {
        public RCI RCI;
        public UTC UTC;
        public CICD CICD;
        public CE CE;
        public UATDD UATDD;
        public FTR FTR;
    }

    [System.Serializable]
    public class GOV
    {
        public SRR SRR;
        public ERR ERR;
        public OA OA;
    }

    [System.Serializable]
    public class RootObject
    {
        public NPS NPS;
        public CS CS;
        public CSM CSM;
        public FTR FTR;
        public UATDD UATDD;
        public CE CE;
        public CICD CICD;
        public UTC UTC;
        public RCI RCI;
        public SRR SRR;
        public ERR ERR;
        public OA OA;
        public VOC VOC;
        public EE EE;
        public GOV GOV;
            
       

    }
}
