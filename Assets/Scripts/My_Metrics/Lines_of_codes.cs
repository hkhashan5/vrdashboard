﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static My_Metrics;

public class Lines_of_codes : MonoBehaviour
{
    public TextMesh lines_of_codes;

    // Start is called before the first frame update
    void Start()
    {
        M_M_Server cds = new M_M_Server();
        RootObject root = cds.CallServer();

        lines_of_codes.text = root.LOC.lines_of_codes;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
