﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane2_Move : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 endPosition;
    float speed = 12.0f;

    void Start()
    {

        endPosition = new Vector3(-30f, 70f, 1100.94f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
    }
}
