﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static My_Metrics;

public class Open_Violation : MonoBehaviour
{
    public TextMesh open_violation;

    // Start is called before the first frame update
    void Start()
    {

        M_M_Server cds = new M_M_Server();
        RootObject root = cds.CallServer();

        open_violation.text = root.OV.open_violation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
