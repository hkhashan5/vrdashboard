﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using static DMI;

public class FTR_Panel : MonoBehaviour
{

    public TextMesh percentage;
    // Start is called before the first frame update
    void Start()
    {
        CallDmiServer cds = new CallDmiServer();
        RootObject root = cds.CallServer();

        percentage.text = root.EE.FTR.percentage;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
